## How to setup ?

Setup docker
* docker-compose up -d

1. Setup tenda_biru
* ```cd tenda_biru```
* Set your mysql config on ./tenda_biru/ormconfig.json
* ```npm install```

2. Setup jualan_juice
* ```cd jualan_juice```
* ```npm install```

3. Setup aneka_soto
* ```cd aneka_soto```
* ```npm install```

## How to run ?

1. Run tenda_biru server
* Make sure you run tenda_biru first, so typeorm can generate table for you
* ```cd tenda_biru```
* ```npm run watch-server```

2. Run aneka_soto
* ```cd aneka_soto```
* ```node index.js```

3. Run jualan_juice
* ```cd jualan_juice```
* ```node index.js```

Code with <3 Fuad
