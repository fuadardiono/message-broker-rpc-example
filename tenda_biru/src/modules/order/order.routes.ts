import * as Router from 'koa-router';
import { Connection } from 'typeorm';
import OrderController from "./order.controller";

class OrderRoutes {
    private routes: Router;
    private socketIo: any;

    constructor(socketIo: any) {
        this.routes = new Router();
        this.socketIo = socketIo;
        const controller = new OrderController(socketIo);

        this.routes.post('/order', controller.processOrder);
    }

    public getRoutes(): Router {
        return this.routes;
    }
}

export default OrderRoutes;
