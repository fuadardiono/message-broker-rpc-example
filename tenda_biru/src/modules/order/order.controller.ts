import * as Koa from 'koa';
import * as amqp from 'amqplib';
import { v4 as uuid } from 'uuid';

let self: OrderController;
class OrderController {
    private sequenceNumberByClient: any;

    constructor(socketIo: any) {
        this.sequenceNumberByClient = socketIo;
        self = this;
    }

    public async processOrder(ctx: Koa.Context) {
        const { body } = ctx.request;
        const amqpCon = await amqp.connect('amqp://investree_dev:root@localhost:5673/vhost');

        try {
            const channel = await amqpCon.createChannel();
            const assertQueue = await channel.assertQueue('', {
                exclusive: true,
                autoDelete: true,
                expires: 5000,
            })

            const corr = uuid();

            body.orders.map((obj: any) => {
                let queueName;
                let information;
                if(obj.retail_name === 'aneka_soto') {
                    queueName = 'aneka_soto_rpc';
                    information = 'Soto sedang dimasak !';
                } else {
                    queueName = 'jualan_juice_rpc';
                    information = 'Juice sedang dimasak !';
                }
                obj.order_uuid = corr;
                const content = Buffer.from(JSON.stringify(obj));
                channel.sendToQueue(queueName, content, {
                    correlationId: corr,
                    replyTo: assertQueue.queue,
                });

                console.log(`Sent ${content}`);

                for (const [client, sequenceNumber] of self.sequenceNumberByClient.entries()) {
                    client.emit("seq-num", { record: null, status: information });
                    self.sequenceNumberByClient.set(client, sequenceNumber + 1);
                }

                channel.consume(assertQueue.queue, (message:any) => {
                    const result = JSON.parse(message.content);
                    console.log(`Receive ${result.message}`);
                    console.log(`Record`, result.record);

                    for (const [client, sequenceNumber] of self.sequenceNumberByClient.entries()) {
                        client.emit("seq-num", { record: result.record[0], status: result.message });
                        self.sequenceNumberByClient.set(client, sequenceNumber + 1);
                    }
                })
            })

            ctx.body = {
                message: "Keren !"
            }
        } catch (error) {
            console.log('Yah error !')
            ctx.body = {
                error
            }
        }
    }
}

export default OrderController;