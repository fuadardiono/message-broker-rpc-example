import {Column, Entity, PrimaryGeneratedColumn, Unique} from 'typeorm';

@Entity('order_table')
export class Order {
	@PrimaryGeneratedColumn()
	public id: number;

	@Column({ type: 'varchar' })
	public order_uuid: string;

	@Column({ type: 'varchar' })
	public food_name: string;

	@Column({ type: 'integer' })
	public quantity: number;

	@Column({ type: 'double' })
	public price: number;

	@Column({ type: 'varchar' })
	public proccessed_from: string;

	@Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
	public created_at: string;

	@Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
	public updated_at: string;

	@Column({ type: 'timestamp', nullable: true })
	public deleted_at: string;
}