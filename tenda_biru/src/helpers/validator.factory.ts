import * as Joi from "joi";
import * as Koa from "koa";

export class ValidatorFactory {
    public static generate(schema: Joi.Schema) {
        return async (ctx: Koa.Context, next: any) => {
            const { body } = ctx.request;
            const { error } =  Joi.validate(body, schema, { abortEarly: false });
            if (!error) {
                await next();
            } else {
                ctx.response.status = 400;
                ctx.body = error.details;
            }
        };
    }
}
