import * as Koa from 'koa';
import * as cors from '@koa/cors';
import * as bodyParser from 'koa-bodyparser';
import * as Router from 'koa-router';
import OrderRoutes from './modules/order/order.routes';
import { Connection, ConnectionOptions, createConnection } from 'typeorm';

createConnection().then((connection: Connection) => {
    const app = new Koa();
    const
    io = require("socket.io"),
    server = io.listen(8000);

    let
    sequenceNumberByClient = new Map();

    // event fired every time a new client connects:
    server.on("connection", (socket:any) => {
        console.info(`Client connected [id=${socket.id}]`);
        // initialize this client's sequence number
        sequenceNumberByClient.set(socket, 1);

        // when socket disconnects, remove it from the list:
        socket.on("disconnect", () => {
            sequenceNumberByClient.delete(socket);
            console.info(`Client gone [id=${socket.id}]`);
        });
    });

    app.use(bodyParser());

    app.use(cors());

    const router = new Router();
    const orderRouter = new OrderRoutes(sequenceNumberByClient);

    router.get('/', async (ctx) => {
        ctx.body = 'Hello Investree from Koa!';
    });

    app.use(orderRouter.getRoutes().routes());
    app.use(router.routes());

    app.listen(3000);

    console.log('Server running on port 3000');
}).catch((err: Error) => {
    throw err;
});