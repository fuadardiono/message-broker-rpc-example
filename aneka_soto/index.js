const amqp = require('amqplib');
const mysql = require('mysql2/promise');
const bluebird = require('bluebird');
const amqpCon = amqp.connect('amqp://investree_dev:root@localhost:5673/vhost');

console.log('Nunggu orderan soto masuk !');

amqpCon
  .then(conn => conn.createChannel())
  .then((ch) => {
    const queue = 'aneka_soto_rpc';
    ch.assertQueue(queue, { durable: false });
    ch.prefetch(1000);
    ch.consume(queue, async (msg) => {
			const content = JSON.parse(msg.content);
			// create the connection, specify bluebird as Promise
			let record;
			try {
				const connection = await mysql.createConnection({host:'localhost', user: 'root', password: '', database: 'tenda_biru', Promise: bluebird});
				query = await connection.query("INSERT INTO order_table (order_uuid,food_name,quantity,price,proccessed_from) VALUES (?,?,?,?,?)",
				[content.order_uuid, content.food_name, content.quantity, content.price, content.retail_name + '_rpcserver']);
				console.log(query);
				[rows, fields] = await connection.query("SELECT * FROM order_table WHERE id = ?", [query[0].insertId]);
			} catch (error) {
				console.log(error);
			}



			const replyWord = {};
			replyWord.message = 'Soto telah selesai dimasak !';
			replyWord.record = rows;
      ch.ack(msg);
      ch.sendToQueue(
        msg.properties.replyTo,
        Buffer.from(JSON.stringify(replyWord)),
        { correlationId: msg.properties.correlationId });

      console.log(replyWord);
    });
  });